# Dev intro

Decentralized Auth is developed as a community-driven project.
Your contributions are appreciated, no matter what size or shape.

Project discussion is currently handled on the [Discord server](https://discord.gg/3aymtSFwcP), which is also bridged to [Matrix](https://matrix.to/#/#decentralized-auth:antonok.com).

Also check the project's [GitLab repo](https://gitlab.com/decentralizedauth/decentralizedauth-fabric) and issue tracker.

The current development roadmap is laid out under the [High level overview](./overview.md) page.
