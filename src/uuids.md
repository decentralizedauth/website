# Avoiding UUID conflicts

Vanilla Minecraft uses Type 4 UUIDs to uniquely identify players.
A primary goal of Decentralized Auth is to avoid unnecessarily fragmenting the existing Minecraft ecosystem by optionally allowing servers which can accept vanilla clients alongside Decentralized Auth clients.
That means it's important to choose UUIDs that are guaranteed not to collide with ones provided by Mojang/Microsoft, while still sharing the same format.

Type 4 UUIDs are randomly generated apart from either 6 or 7 bits to indicate the type and variant:

```
b876ec32-e396-476b-a115-8438d83c67d4
              ^
              always `4` in a version 4 UUID
```

Type 5 UUIDs, on the other hand, are generated based on a SHA-1 hash of the content they identify.
Content-addressing is a great fit for decentralized systems like [IPFS](https://ipfs.io), so Type 5 UUIDs are a great fit for Decentralized Auth.

The type field in Type 5 UUIDs is always a `5`, so there's a guarantee of no collisions!

```
04e0eded-79f2-5f74-bbac-91aeb50cf540
              ^
              always `5` in a version 5 UUID
```

The namespace selected for Decentralized Auth player UUIDs is `588672c8-7f77-43fc-98e7-0413caee61d4`.

SHA-1 itself is no longer cryptographically secure against collisions on specially-crafted input pairs. This should be mitigated by having each player generate their own secure keypair using a modern asymmetric encryption algorithm, such that the public key is sufficiently unpredictable and relatively expensive to brute force. A player's Decentralized Auth UUID should only ever be derived from a public key after verifying that they have access to its corresponding private key.
