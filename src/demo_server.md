# Demo server

Decentralized Auth has an official demo server at `mc.decentralizedauth.net`.
This demo server is running Minecraft 1.19 with the latest release of Decentralized Auth on [Modrinth](https://modrinth.com/mod/decentralizedauth/version/1.0.0-beta1%2B1.19).

## Whitelist access

There is a whitelist on the server.
If you'd like to gain access, please inquire on our chatroom using [Discord](https://discord.gg/3aymtSFwcP) or [Matrix](https://matrix.to/#/#decentralized-auth:antonok.com).
Include the UUID of the player you'd like to have added to the whitelist, and/or your official account's username if you'd like to connect with your official account.
For now, you can find your player's UUID by entering `/decentralizedauth player` in a singleplayer world with cheats enabled.

## Server info

It's a small 4000x4000 world with a focus on testing and community building for anyone interested in Decentralized Auth.

Please read the rules at spawn once you've joined.
Your account may be removed from the whitelist if you break any rules.
