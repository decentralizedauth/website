# User guide

> <span style="color:red">**Warning**</span>: Decentralized Auth is currently in Beta, so there may be breaking protocol changes in the future.
> Proceed with caution and prepare to have your server and client mod versions updated together!

Decentralized Auth is currently released for [Fabric 1.19 on Modrinth](https://modrinth.com/mod/decentralizedauth).

## Fabric client instructions

1. Install the [Fabric Loader](https://fabricmc.net/use/installer/), and create a modded Minecraft 1.19 version. This is required for the official Minecraft Launcher, although other launchers may have it installed by default.
2. Install the [Fabric API for Minecraft 1.19](https://modrinth.com/mod/fabric-api/version/0.58.0%2B1.19) by placing the JAR file in your `.minecraft/mods` folder.
3. Install [Decentralized Auth for Minecraft 1.19](https://modrinth.com/mod/decentralizedauth/version/1.0.0-beta1%2B1.19) by placing the JAR file in your `.minecraft/mods` folder.
4. Start your modded Minecraft 1.19 install. You should be able to connect to Decentralized Auth servers now, such as the [public demo server](/demo_server.md).

## Fabric server instructions

1. Install the [Fabric Server Launcher](https://fabricmc.net/use/server/).
2. Install the [Fabric API for Minecraft 1.19](https://modrinth.com/mod/fabric-api/version/0.58.0%2B1.19) by placing the JAR file in your server's `mods` folder.
3. Install [Decentralized Auth for Minecraft 1.19](https://modrinth.com/mod/decentralizedauth/version/1.0.0-beta1%2B1.19) by placing the JAR file in your server's `mods` folder.
4. Start your modded Minecraft 1.19 install. You should be able to connect to Decentralized Auth servers now, such as the [public demo server](/demo_server.md).

## Customizing your player

You can add a skin to your Decentralized Auth player by saving a valid skin texture to `.minecraft/decentralizedauth/client/SKIN.png` in your modded client version's data directory.

You can also add a cape texture by saving it to `.minecraft/decentralizedauth/client/CAPE.png`.
If you're looking for a cape texture to use, you can try the official [Decentralized Auth cape](/CAPE.png)!

## Other mod loaders and Minecraft versions

Support for other mod loaders and versions is planned but not yet started.
If you'd like to see a particular setup prioritized, please add a `👍` to the corresponding issue(s) on GitLab, or create a new issue if one doesn't exist.
