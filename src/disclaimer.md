# Disclaimer

## On piracy

Decentralized Auth is not intended as a piracy tool, and should only be used in an official Minecraft client or server release by players who have legitimately purchased Minecraft.

That being said, if your legitimate Minecraft account was banned as a result of the chat reporting features introduced in 1.19.1, you still own a license to the game, so you'd have a decent case for using this to connect to compatible servers. We'd still recommend talking to your lawyer though.

## On being an unofficial tool

The Decentralized Auth developers are in no way affiliated with Mojang or Microsoft, and Decentralized Auth is not an official Minecraft modification.
