# Summary

- [Decentralized Auth](./index.md)
- [Motivation](./motivation.md)
- [Disclaimer](./disclaimer.md)

# For Users
- [User guide](./user_guide.md)
- [Official demo server](./demo_server.md)

# For Developers
- [Dev intro](./dev_intro.md)
- [High level overview](./overview.md)
- [Avoiding UUID conflicts](./uuids.md)
- [Username policies](./username_policies.md)
- [Skin textures](./skin_textures.md)
- [Protocol](./protocol.md)
