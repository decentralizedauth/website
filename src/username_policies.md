# Username policies

On vanilla servers, players have unique UUIDs _and_ usernames.
They're free to change their username by requesting a new one from Microsoft, but Microsoft ensures that there are never any duplicate usernames (although there have been rare duplicate name exploits in the past).

The core mechanism of Decentralized Auth is cryptographic protection against duplicate UUIDs, but _not_ protection against duplicate usernames.
While Minecraft still works with duplicated usernames, it can be extremely confusing for onlookers, and many admin commands will select a single arbitrary player with the provided username without providing any mechanism for choosing by UUID instead.
Most server admins will be interested in using a policy to enforce unique usernames.

Decentralized Auth is a player authentication system and not a username assignment system, but the problem of duplicate usernames is important, common, and specific to alternative authentication systems, so Decentralized Auth provides some basic tools for dealing with it.
However, usernames are complicated and there are many possible ways that a server admin may wish to manage usernames.
Ultimately, the intention is for Decentralized Auth to expose a generic username policy API such that plugin developers can create, share, and deploy their own custom logic for username management.

This document provides some technical details about username management using Decentralized Auth, as well as some examples of policies that server admins may wish to implement or adopt.

## Bare minimum policy

The first release of Decentralized Auth will provide a very minimal set of serverside config for username management, taking advantage of the vanilla `UserCache`.
Improvements to this policy should not affect the network protocol at all, so it should be possible to perform backward-compatible upgrades in the future.

1. Allow username changes: true/false (default false)
    - If true, the server will request the player's username each time they join.
    - If false, the server will always attempt to pull the player's username from the `UserCache` whenever they join the server, only requesting a new username if one cannot be found.
2. Prevent duplicate usernames: true/false (default true)
    - If true, the server will always check the `UserCache` for players with the requested username when a player joins a server.
      If another player has already logged in with the requested username previously, the new player will be denied login.
    - If false, the server will always allow users to login with their requested username.
      This is generally not recommended.
3. Precedence to official Minecraft account usernames: "strict"/"prefer"/"none" (default "prefer")
    - "strict": It is impossible for a Decentralized Auth player to join a server with a username currently used by any official Minecraft account.
    - "prefer": Decentralized Auth players may choose a username of an official Minecraft account, but they will lose the rights to that name if and when the official account joins the server for the first time.
    - "none": Official Minecraft account usernames get no special treatment. Official Minecraft accounts may be unable to join the server while a Decentralized Auth player has the same name.

Note that the vanilla `UserCache` expires cached profiles after 1 month, after which point a username can be reclaimed or changed without restrictions.

## Policy examples

The following assortment of username management policy options is intended to demonstrate the complexity of username management, and serve as a reference when designing a more flexible policy API.
Note that several of these options could be in use simultaneously.
If you have a use-case that isn't described here, please open an issue or PR to add it.

### Reserve on first use

The server requests a player's username when they join the server for the first time, at which point that username will be immediately reserved to that player's UUID.

### Revoke when using a new name

If a player with an existing username reservation ever joins the server with a different username, the previous username reservation will be removed so that the original username can be used by another player.

### Reservation expiration

Username reservations optionally expire after a configurable amount of offline time to mitigate name-squatting (since Decentralized Auth identities are essentially "free" to create).

### Official Minecraft account usernames

Precedence can optionally be given to official Minecraft account usernames in a few different ways, as described above.

### By playtime

Whenever a player joins a server with a particular username, they begin to accumulate "stake" in the username proportional to the amount of time they are online with that username.
The username is reserved for the player above a minimum threshold of stake.
Stake may also optionally decay or grow over offline time at a certain rate.

### External registration

Server admins may operate a website where players must sign up for an account in order to be whitelisted on the server.
They can tie the usernames on the Minecraft server to username registrations on the website.
This might involve any kind of logic, possibly including payment for a username reservation as a premium feature.

### Fully decentralized username service

A server admin may choose to only respect username reservations posted on a decentralized ledger, such as a blockchain.
