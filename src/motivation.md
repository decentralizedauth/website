# Motivation

Many of Minecraft's oldest players purchased the game to be a part of its vibrant community.
This was a community which grew around the freedom provided by Mojang.
Freedom to play however you wanted, freedom to build however you wanted, freedom to mod the game however you wanted, and most importantly the freedom to share all of that with your friends by running your own server however you wanted.

## Turns for the worse

In 2014, Mojang released an updated End User License Agreement for Minecraft.
This EULA prevented servers from offering any advantages whatsoever in exchange for real-world currency.
The change removed a significant source of income for many server administrators, many of whom could no longer afford to operate their servers and had to shut them down.
This was a devastating blow to the community.
Yet, some server owners adapted and continued to operate under the more restrictive rules, despite the difficulties involved.

## Microsoft's acquisition

Microsoft acquired Mojang later in 2014 and begun fracturing the playerbase further with a heavy development focus and subsequent pushing of players towards Minecraft Bedrock Edition.

Not only is Bedrock a completely separate platform that is incompatible with the original game (since dubbed the Java Edition), but it's much less open to modding.

Modding has been an essential part of Minecraft's popularity, truly allowing players to do anything they could imagine.
When Bedrock Edition 1.0.0 was officially released in 2016, Minecraft had already built up a tremendous modding community in terms of developers, modpack creators, and users.
None of those efforts can be transferred to Bedrock Edition, which built in C++, a foundation that has proven much harder for mod developers to work with.

## Chat reporting

Today, Microsoft is developing a chat reporting feature for Minecraft, forcing all chat messages to be cryptographically signed by a key identifying your user account.
Upon receiving a report of an inappropriate chat message, Microsoft will see the message and its context, and will be able to link that message uniquely to your Microsoft account.
The chat reporting system is integrated with the newly enforced Microsoft authentication system, such that Microsoft can unilaterally ban players from logging in to _any_ multiplayer servers.

Once again, Microsoft is imposing new _global_ policies to restrict how individual server operators and players are allowed to use the game.
Despite the game's history of success as a decentralized, community-led effort.
Despite Big Tech's continued demonstration that moderation doesn't work at scale.

## A solution?

It may not be too late to put control back in the hands of the community.
Minecraft has always had an impressive modding ecosystem, often pioneering the state-of-the-art in Java decompilation and code injection.

If we can decouple the Minecraft server and client implementations from Microsoft's authentication servers altogether, we can save the community from future attacks.
A decentralized authentication system also opens the door to cleanroom third-party server and client implementations that can be fully interoperable with official implementations, and yet no longer subject to Microsoft's EULA enforcements.

#### Let's do it!
