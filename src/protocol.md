# Protocol

This page provides technical details about the packet formats introduced by Decentralized Auth.
The format is heavily inspired by [wiki.vg's Protocol document](https://wiki.vg/Protocol).

## Handshaking

See [wiki.vg's Handshaking protocol document](https://wiki.vg/Protocol#Handshaking).
Decentralized Auth reuses the same packet but sets `Next State` to a value of `69` to indicate a transfer to the Decentralized Auth flow.

## Decentralized Auth

The Decentralized Auth login process is as follows:

1. C→S: Handshake with Next State set to 69 (Decentralized Auth)
2. C→S: Decentralized Auth Start
3. S→C: Encryption Request
4. C→S: Encryption Response
5. Client and server enable encryption
6. S→C: Auth Challenge
7. C→S: Auth Proof
8. Server enforces banlist/whitelist by IP and UUID
9. S→C: Profile Request
10. C→S: Profile Response
11. Server runs login hook based on the requested profile
12. S→C: Set Compression (optional)
13. S→C: Decentralized Auth Success

### Clientbound

#### Disconnect

```extended-markdown-table
| Packet ID |       State        | Bound To | Field Name | Field Type |                    Notes                   |
|-----------|--------------------|----------|------------|------------|--------------------------------------------|
|      0x00 | Decentralized Auth | Client   | Reason     | Chat       | The reason why the player was disconnected |
```

#### Encryption Request

```extended-markdown-table
| Packet ID |       State        | Bound To |     Field Name    | Field Type |                    Notes                       |
|-----------|--------------------|----------|-------------------|------------|------------------------------------------------|
|      0x01 | Decentralized Auth | Client   | Public Key Length | VarInt     | Length of Public Key                           |
|           |                    |          |-------------------|------------|------------------------------------------------|
|           |                    |          | Public Key        | Byte Array | Server's ephemeral X25519 public key, in bytes |
```

#### Auth Challenge

```extended-markdown-table
| Packet ID |       State        | Bound To |           Field Name           | Field Type |                    Notes                         |
|-----------|--------------------|----------|--------------------------------|------------|--------------------------------------------------|
|      0x02 | Decentralized Auth | Client   | Identity Public Key Length     | VarInt     | Length of Identity Public Key                    |
|           |                    |          |--------------------------------|------------|--------------------------------------------------|
|           |                    |          | Identity Public Key            | Byte Array | Server's Ed25519 Identity Public key, in bytes   |
|           |                    |          |--------------------------------|------------|--------------------------------------------------|
|           |                    |          | Length of signed shared secret | VarInt     | Length of the signed shared secret               |
|           |                    |          |--------------------------------|------------|--------------------------------------------------|
|           |                    |          | Signed shared secret           | Byte Array | Shared secret signed by the identity Ed25519 key |
```

#### Profile Request

```extended-markdown-table
| Packet ID |       State        | Bound To |     Field Name       |       Field Type               |                    Notes                        |
|-----------|--------------------|----------|----------------------|--------------------------------|-------------------------------------------------|
|      0x03 | Decentralized Auth | Client   | Has Profile Data     | Boolean                        | Whether or not the remaining fields are present |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------|
|           |                    |          | UUID                 | UUID                           |                                                 |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------|
|           |                    |          | Username             | String (16)                    |                                                 |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------|
|           |                    |          | Number of Properties | VarInt                         | Number of elements in the following array       |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------|
|           |                    |          | Property | Name      | Array | String(32767)          |                                                 |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------|
|           |                    |          |          | Value     |       | String(32767)          |                                                 |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------|
|           |                    |          |          | Is Signed |       | Boolean                | Generally false for Decentralized Auth          |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------|
|           |                    |          |          | Signature |       | Optional String(32767) | Only if Is Signed is true                       |
```

#### Compression Request

This is the same as the vanilla Compression Request packet.
The Packet ID is 0x04.

#### Login Success

```extended-markdown-table
| Packet ID |       State        | Bound To |     Field Name       |       Field Type               |                    Notes                  |
|-----------|--------------------|----------|----------------------|--------------------------------|-------------------------------------------|
|      0x05 | Decentralized Auth | Client   | UUID                 | UUID                           |                                           |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------|
|           |                    |          | Username             | String (16)                    |                                           |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------|
|           |                    |          | Number of Properties | VarInt                         | Number of elements in the following array |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------|
|           |                    |          | Property | Name      | Array | String(32767)          |                                           |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------|
|           |                    |          |          | Value     |       | String(32767)          |                                           |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------|
|           |                    |          |          | Is Signed |       | Boolean                | Generally false for Decentralized Auth    |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------|
|           |                    |          |          | Signature |       | Optional String(32767) | Only if Is Signed is true                 |
```

### Serverbound

#### Decentralized Auth start

```extended-markdown-table
| Packet ID |       State        | Bound To |     Field Name    | Field Type |      Notes     |
|-----------|--------------------|----------|-------------------|------------|----------------|
|      0x00 | Decentralized Auth | Server   |                   |            | (empty packet) |
```

#### Encryption Response

```extended-markdown-table
| Packet ID |       State        | Bound To |     Field Name    | Field Type |                      Notes                     |
|-----------|--------------------|----------|-------------------|------------|------------------------------------------------|
|      0x01 | Decentralized Auth | Server   | Public Key Length | VarInt     | Length of Public Key                           |
|           |                    |          |-------------------|------------|------------------------------------------------|
|           |                    |          | Public Key        | Byte Array | Client's ephemeral X25519 public key, in bytes |
```

#### Auth Proof

```extended-markdown-table
| Packet ID |       State        | Bound To |           Field Name           | Field Type |                      Notes                       |
|-----------|--------------------|----------|--------------------------------|------------|--------------------------------------------------|
|      0x02 | Decentralized Auth | Server   | Identity Public Key Length     | VarInt     | Length of Identity Public Key                    |
|           |                    |          |--------------------------------|------------|--------------------------------------------------|
|           |                    |          | Identity Public Key            | Byte Array | Server's Ed25519 Identity Public key, in bytes   |
|           |                    |          |--------------------------------|------------|--------------------------------------------------|
|           |                    |          | Length of signed shared secret | VarInt     | Length of the signed shared secret               |
|           |                    |          |--------------------------------|------------|--------------------------------------------------|
|           |                    |          | Signed shared secret           | Byte Array | Shared secret signed by the identity Ed25519 key |
```

#### Profile Response

```extended-markdown-table
| Packet ID |       State        | Bound To |      Field Name      |          Field Type            |                        Notes                          |
|-----------|--------------------|----------|----------------------|--------------------------------|-------------------------------------------------------|
|      0x03 | Decentralized Auth | Server   | UUID                 | UUID                           |                                                       |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------------|
|           |                    |          | Username             | String (16)                    |                                                       |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------------|
|           |                    |          | Number of Properties | VarInt                         | Number of elements in the following array             |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------------|
|           |                    |          | Property | Name      | Array | String(32767)          |                                                       |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------------|
|           |                    |          |          | Value     |       | String(32767)          |                                                       |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------------|
|           |                    |          |          | Is Signed |       | Boolean                | Generally false for Decentralized Auth                |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------------|
|           |                    |          |          | Signature |       | Optional String(32767) | Only if Is Signed is true                             |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------------|
|           |                    |          | Number of Textures   | VarInt                         | Number of elements in the following array             |
|           |                    |          |----------------------|--------------------------------|-------------------------------------------------------|
|           |                    |          | Property | Type      | Array | String(32767)          | Only "SKIN" and "CAPE" are valid                      |
|           |                    |          |          |-----------|       |------------------------|-------------------------------------------------------|
|           |                    |          |          | Value     |       | Byte Array(16384)      | PNG data matching any texture SHA256 hashes requested |
```

## Play

Note that the Play packets for Decentralized Auth are implemented as [plugin messages](https://wiki.vg/Plugin_channels).

### Clientbound

#### Texture Response

```extended-markdown-table
|       Plugin Channel       | State | Bound To |  Field Name   | Field Type |                      Notes                     |
|----------------------------|-------|----------|---------------|------------|------------------------------------------------|
| decentralizedauth:textures | Play  | Client   | SHA256 Length | VarInt     | Length of SHA256 hash                          |
|                            |       |          |---------------|------------|------------------------------------------------|
|                            |       |          | SHA256        | Byte Array | SHA256 hash in bytes                           |
```

### Serverbound

#### Texture Request

```extended-markdown-table
|       Plugin Channel       | State | Bound To |     Field Name      | Field Type |                      Notes                     |
|----------------------------|-------|----------|---------------------|------------|------------------------------------------------|
| decentralizedauth:textures | Play  | Server   | Texture Data Length | VarInt     | Length of Texture Data                         |
|                            |       |          |---------------------|------------|------------------------------------------------|
|                            |       |          | Texture Data        | Byte Array | Texture Data in bytes                          |
```
