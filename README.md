# Decentralized Auth website

This website is built using [mdBook](https://rust-lang.github.io/mdBook/).

## License

This software and its associated documentation are released into the public domain by the copyright holders.
